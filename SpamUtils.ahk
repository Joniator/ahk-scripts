#SingleInstance, force
SetTitleMatchMode, 2
SendMode Input
displaySplash := false

delay := 150                  ; Ctrl + Alt + NumpadAdd/NumpadSub
spamMcAttack := false         ; Ctrl + Alt + LButton
spamHunie := false            ; Ctrl + Alt + LButton
spamLolMastery := false       ; Ctrl + Alt + LButton

loop {
  if displaySplash {
    continue
  }

  if spamMcAttack 
    if WinActive("Minecraft")
      Send {LButton}
    else
      spamMcAttack := false

  if spamHunie 
    if WinActive("HunieCam Studio")
      Send {LButton}
    else
      spamHunie := false

  if spamLolMastery
    if WinActive("League of Legends")
      Send ^6
    else
      spamLolMastery := false

  Sleep delay
}

;;;
; Script Controls
;;;
ShowIntervalSplash:
  displaySplash := true
  SplashTextOn, , , "Interval: %delay%ms"
  SetTimer, SplashTextOff, 350
return

SplashTextOff: 
  SplashTextOff
  displaySplash := false
return

^!NumpadEnter::Pause,Toggle

^!NumpadSub::
  if delay < 1500 
    delay += 150
  GoSub, ShowIntervalSplash
return

^!NumpadAdd::
  if delay > 150 
    delay -= 150
  GoSub, ShowIntervalSplash
return

;;;
; Minecraft
;;;
#IfWinActive, Minecraft
^!LButton:: spamMcAttack := !spamMcAttack

;;;
; HunieCam Studio
;;;
#IfWinActive, HunieCam Studio
^!Space:: spamHunie := !spamHunie

;;;
; League of Legends
;;;
#IfWinNotActive, League of Legends
^!Space:: spamLolMastery := !spamLolMastery